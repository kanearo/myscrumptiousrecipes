from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from .models import Recipe
from .forms import RecipeForm, PostForm
from .spoonacular import spoonacular_search
from rest_framework import generics
from .models import Recipe
from .serializers import RecipeSerializer
import requests
from django.conf import settings
from django.shortcuts import render

def search_recipes(query):
    url = "https://api.spoonacular.com/recipes/complexSearch"
    params = {
        "apiKey": settings.SPOONACULAR_API_KEY,
        "query": query,
        "number": 10,
        "addRecipeInformation": "true",
        "fillIngredients": "true"
    }
    response = requests.get(url, params=params)
    if response.status_code == 200:
        data = response.json()
        recipe_list = []
        for recipe_data in data["results"]:
            recipe = {
                "title": recipe_data["title"],
                "picture": recipe_data["image"],
                "source_url": recipe_data["sourceUrl"],
                "servings": recipe_data["servings"],
                "ingredients": [ingredient.get("originalString", "") for ingredient in recipe_data.get("extendedIngredients", [])],
                "instructions": recipe_data.get("instructions", ""),
            }
            recipe_list.append(recipe)
        return recipe_list
    else:
        return None






class RecipeListAPIView(generics.ListAPIView):
    queryset = Recipe.objects.all()
    serializer_class = RecipeSerializer

class RecipeDetailAPIView(generics.RetrieveAPIView):
    queryset = Recipe.objects.all()
    serializer_class = RecipeSerializer



# views.py





def show_recipe(request):
    search_term = request.GET.get("search_term")
    if search_term:
        recipe_list = search_recipes(search_term)
    else:
        recipe_list = []
    context = {"recipe_list": recipe_list}
    return render(request, "recipes/list.html", context)




def show_recipe_detail(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == 'POST':
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect(reverse('recipe_detail', args=[recipe.id]))
    else:
        form = PostForm(request.POST or None, instance=post)

    context = {
        'recipe_object': recipe,
        'recipe_form': form,
    }
    return render(request, 'recipes/detail.html', context)


def home(request):
    return render(request, 'home.html')

