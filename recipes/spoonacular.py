import requests

def spoonacular_search(query):
    url = "https://api.spoonacular.com/recipes/complexSearch"
    params = {
        "apiKey": "24b85216912f44d496bc65f3882263e4",
        "query": query
    }
    response = requests.get(url, params=params)
    if response.status_code == 200:
        return response.json()
    else:
        return None
